# README #



### How do I use the script ? ###

* Download the production ready [Formula1.jar](https://bitbucket.org/paolo_bruzzo/formula1/src/e94932c296ca2dd7592c65cef30ff0a12f44f080/out/artifacts/Formula1_jar/?at=master)
* Execute it on the console typing:$ **java -jar path_to/Formula1.jar 10 1000.0** (this will run a 1000 meters long race of 10 teams. Change the parameters as you like)

### Requirements ###
* The script runs on JRE 8+, it has not been tested on earlier versions.
* If you wish to run the [source code](https://bitbucket.org/paolo_bruzzo/formula1/src/e94932c296ca2dd7592c65cef30ff0a12f44f080/src/?at=master), or the [Unit Tests](https://bitbucket.org/paolo_bruzzo/formula1/src/e94932c296ca2dd7592c65cef30ff0a12f44f080/src/UnitTests/?at=master), JDK 8+ and JUnit 4 are required.

### Assumptions based on the description ###
* Handling and Nitro produce an instant speed change (dv/dt -> infinite), and are applied at the beginning of each assessment (but not at the start)
* Handling speed reduction only applies if there is another car closer than 10 meters
* Handling speed reduction occurs before the nitro, and they both can happen during the same assessment for the same car
* The position of the first car is on the start line. (The following cars so will be at -200, -600, -1200 etc...)
* Even if the assessment are done every 2 seconds, final time and speed calculation refer exactly to the ones when the car crossed the line.