package Elements;

import java.util.ArrayList;

public class Driver {
    /*=======================================
     * ATTRIBUTES
     *=======================================*/
    private double nearbyPosition; //Meters

    /*=======================================
     * CONSTRUCTORS
     *=======================================*/
    public Driver() {
        this.nearbyPosition = 10.0;
    }

    /*=======================================
     * PRIVATE METHODS
     *=======================================*/

    /*=======================================
     * GETTERS AND SETTERS
     *=======================================*/

    /*=======================================
     * PUBLIC METHODS
     *=======================================*/

    /**
     * Checks if (s)he is last
     *
     * @param currentClassification the ordered list of the running teams
     * @param myTeam                team of the driver
     * @return true if the driver is last
     * @throws IllegalArgumentException when the driver is not racing, but is still asked to check
     */
    public boolean iAmLast(ArrayList<Team> currentClassification, Team myTeam) throws IllegalArgumentException {
        int myPosition = currentClassification.indexOf(myTeam);
        if (myPosition == -1)
            throw new IllegalArgumentException("I'm not racing !");

        return (myPosition + 1) == currentClassification.size();
    }

    /**
     * Checks if there is any nearby car
     *
     * @param currentClassification all the cars
     * @param myTeam                the team of the driver
     * @return true if there is at least 1 car nearby
     * @throws IllegalArgumentException when I'm not in the list of the running cars, but still I'm asked to check
     */
    public boolean iHaveNearbyCars(ArrayList<Team> currentClassification, Team myTeam) throws IllegalArgumentException {
        int myPosition = currentClassification.indexOf(myTeam);
        if (myPosition == -1)
            throw new IllegalArgumentException("I'm not racing !");

        // If there are less than 2 people in the race
        if (currentClassification.size() < 2)
            return false;

        // If I'm the first one, just check the car after me (even if it already crossed the finish line)
        if (myPosition == 0)
            return Math.abs(currentClassification.get(1).getCurrentPosition() - myTeam.getCurrentPosition()) <= this.nearbyPosition;

        // If I'm the last one, just check the car before me
        if (myPosition == currentClassification.size() - 1)
            return Math.abs(myTeam.getCurrentPosition() - currentClassification.get(myPosition - 1).getCurrentPosition()) <= this.nearbyPosition;

        // Otherwise check both before and after
        return Math.abs(currentClassification.get(myPosition + 1).getCurrentPosition() - myTeam.getCurrentPosition()) <= this.nearbyPosition ||
                Math.abs(myTeam.getCurrentPosition() - currentClassification.get(myPosition - 1).getCurrentPosition()) <= this.nearbyPosition ;
    }
}
