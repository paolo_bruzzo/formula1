package Elements;

import Logic.Utils.Physics;

public class Car {
    /*=======================================
     * ATTRIBUTES
     *=======================================*/
    private double topSpeed;
    private double acceleration;
    private double handlingFactor;
    private boolean nitroUsed;
    private double currentSpeed;

    /*=======================================
     * CONSTRUCTORS
     *=======================================*/

    /**
     * Constructor of the car
     *
     * @param teamId is the number of the team
     */
    public Car(int teamId) {
        this.topSpeed = (150.0 + 10 * teamId) / 3.6; // Converted to m/s
        this.acceleration = (2.0 * teamId); // m/s^2
        this.handlingFactor = 0.8;
        this.nitroUsed = false;
        this.currentSpeed = 0; // in m/s
    }

    /*=======================================
     * PRIVATE METHODS
     *=======================================*/

    /* Accelerated motion speed formula */
    private double calculateNewSpeed(double intervalLength) {
        return Math.min(this.topSpeed, this.currentSpeed + intervalLength * this.acceleration);
    }

    /*=======================================
     * GETTERS AND SETTERS
     *=======================================*/

    /**
     * Get the top speed
     *
     * @return the top speed in m/s
     */
    public double getTopSpeed() {
        return this.topSpeed;
    }

    /**
     * Get the car acceleration
     *
     * @return the acceleration in m/s^2
     */
    public double getAcceleration() {
        return this.acceleration;
    }

    /**
     * Get the car handling factor
     *
     * @return the handling factor
     */
    public double getHandlingFactor() {
        return this.handlingFactor;
    }

    /**
     * Get the current speed
     *
     * @return the current speed in m/s
     */
    public double getCurrentSpeed() {
        return this.currentSpeed;
    }

    /**
     * Tells if the car can use the nitro
     *
     * @return true if the car is allowed to use the nitro
     */
    public boolean canUseNitro() {
        return !nitroUsed;
    }

    /*=======================================
     * PUBLIC METHODS
     *=======================================*/

    /**
     * Apply handling factor due to driver distraction
     */
    public void applyHandling() {
        this.currentSpeed = this.currentSpeed * this.handlingFactor;
    }

    /**
     * Apply the nitro
     */
    public void applyNitro() {
        if (!canUseNitro())
            throw new IllegalStateException("The nitro cannot be applied");
        this.currentSpeed = Math.min(this.topSpeed, this.currentSpeed * 2);
        this.nitroUsed = true;
    }

    /**
     * Calculates how much space the car runs, starting with a speed of {@link #currentSpeed}
     * for the following intervalLength seconds.
     *
     * @param intervalLength is the length of the interval in seconds
     * @return the space run in meters
     */
    public double computeSpaceRun(double intervalLength) {
        if (intervalLength <= 0)
            throw new IllegalArgumentException("The checks can be done in an interval strictly grater than 0 seconds ");

        double sConstantAcc = 0.0; // Space done accelerating
        double sConstantSpeed = 0.0; // Space done at constant speed

        // Object for physics resolution
        Physics p = new Physics();
        double accelerationTime = p.timeToConstantSpeed(this.topSpeed, this.currentSpeed, this.acceleration);


        // The car will accelerate for the entire interval
        if (accelerationTime >= intervalLength) {
            sConstantAcc = p.spaceAccelarated(intervalLength, this.currentSpeed, this.acceleration);
        }
        // The car will accelerate for a while, and the move with constant top speed
        else if (accelerationTime < intervalLength && accelerationTime > 0) {
            double constSpeedTime = intervalLength - accelerationTime;

            sConstantAcc = p.spaceAccelarated(accelerationTime, this.currentSpeed, this.acceleration);
            this.currentSpeed = calculateNewSpeed(accelerationTime);
            sConstantSpeed = p.spaceAtConstantSpeed(constSpeedTime, this.currentSpeed);
        }
        // The car will move with constant top speed for the entire interval
        else {
            sConstantSpeed = p.spaceAtConstantSpeed(intervalLength, this.currentSpeed);
        }

        this.currentSpeed = calculateNewSpeed(intervalLength);
        return sConstantAcc + sConstantSpeed;
    }

}
