package Elements;


import Logic.Utils.Physics;

import java.util.ArrayList;
import java.util.regex.Matcher;

public class Team {
    /*=======================================
     * ATTRIBUTES
     *=======================================*/
    private int id;
    private Car car;
    private Driver driver;
    private double currentPosition;
    private double finalTime;

    /*=======================================
     * CONSTRUCTORS
     *=======================================*/

    /**
     * Constructor of the team class
     *
     * @param gridPosition is the starting position number of the team after the qualifications
     * @throws IllegalArgumentException when gridPosition is < 1
     */
    public Team(int gridPosition) throws IllegalArgumentException {
        if (gridPosition < 1)
            throw new IllegalArgumentException("The first position on the start line must be number 1");
        this.id = gridPosition;
        this.currentPosition = -(gridPosition * (gridPosition-1) * 100.0); // meters from the start line
        this.car = new Car(this.id);
        this.driver = new Driver();
        this.finalTime = 0.0;
    }

    /*=======================================
     * PRIVATE METHODS
     *=======================================*/

    /*=======================================
     * GETTERS AND SETTERS
     *=======================================*/

    /**
     * Get the current position of the team
     *
     * @return the current position in meters from the start line
     */
    public double getCurrentPosition() {
        return this.currentPosition;
    }

    /**
     * Get the current time of the team
     *
     * @return the current time in seconds from the start line
     */
    public double getCurrentTime() {
        return this.finalTime;
    }

    /**
     * Get the driver of this team
     *
     * @return the current driver
     */
    public Driver getDriver() {
        return this.driver;
    }

    /**
     * Get the car of this team
     *
     * @return the current car
     */
    public Car getCar() {
        return this.car;
    }

    /*=======================================
     * PUBLIC METHODS
     *=======================================*/

    @Override
    public String toString() {
        return "Team " + this.id + " crossed the line in " +
                String.format("%.3f", this.finalTime) + " seconds, at " +
                String.format("%.1f", this.car.getCurrentSpeed() * 3.6) + " Km/h";
    }

    /**
     * Increments the current position based on the next interval
     *
     * @param trackLength            is the length of the track
     * @param nextAssessmentInterval is the time interval in between
     */
    public void calculateNewPosition(double trackLength, double nextAssessmentInterval) {

        Physics p = new Physics();
        double accelerationTime = p.timeToConstantSpeed(car.getTopSpeed(), car.getCurrentSpeed(), car.getAcceleration());
        double distanceToFinishLine = trackLength - this.getCurrentPosition();
        double theoreticalTimeToFinish;

        // 1) The car is already moving linear
        if (accelerationTime == 0) {
            theoreticalTimeToFinish = p.timeToPositionAtConstantSpeed(distanceToFinishLine, car.getCurrentSpeed());
        } else {
            double theoreticalFinalSpeed = p.speedFinalInAcceleratedMotion(distanceToFinishLine, car.getAcceleration(), car.getCurrentSpeed());
            // 2) The car is still accelerating but will reach the finish line at linear speed
            if (theoreticalFinalSpeed > car.getTopSpeed()) {
                double accSpace = p.spaceAccelarated(accelerationTime, car.getCurrentSpeed(), car.getAcceleration());
                double linearTime = p.timeToPositionAtConstantSpeed(distanceToFinishLine - accSpace, car.getTopSpeed());
                theoreticalTimeToFinish = accelerationTime + linearTime;
            }
            // 3) The car will cross the line in acceleration
            else {
                theoreticalTimeToFinish = p.timeToPositionInAcceleration(distanceToFinishLine, car.getCurrentSpeed(), car.getAcceleration());
            }
        }

        // The car will run the minimum in between the next assessment time, or the time taken to complete the race
        double runInterval = Math.min(theoreticalTimeToFinish, nextAssessmentInterval);

        // Update values
        this.finalTime += runInterval;
        this.currentPosition += car.computeSpaceRun(runInterval);
    }

    /**
     * Tells the driver to check his current position
     *
     * @param currentClassification is the list of the teams sorted
     */
    public void checkPosition(ArrayList<Team> currentClassification) {
        //Check if other cars are close
        if (this.driver.iHaveNearbyCars(currentClassification, this)) {
            this.car.applyHandling();
        }

        //Check if the driver is last
        if (this.driver.iAmLast(currentClassification, this) && this.car.canUseNitro()) {
            this.car.applyNitro();
        }
    }

}
