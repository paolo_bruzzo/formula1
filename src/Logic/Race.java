package Logic;

import Elements.Team;
import Logic.Utils.TeamsComparator;

import java.util.*;

import static java.lang.System.exit;

public class Race {
    /*=======================================
     * ATTRIBUTES
     *=======================================*/
    private int numberOfTeams;
    private List<Team> teams;
    private double trackLength; //meters
    private double assessmentInterval; //seconds
    private List<Team> finalClassification;

    /*=======================================
     * CONSTRUCTORS
     *=======================================*/

    /**
     * Contructor of the Race class
     *
     * @param numberOfTeams teams involved in the race
     * @param trackLength   meters of the track
     * @throws IllegalArgumentException when number of teams < 2 or trackLength <=0
     */
    public Race(int numberOfTeams, double trackLength) throws IllegalArgumentException {
        if (numberOfTeams < 2)
            throw new IllegalArgumentException("At least 2 teams are needed to start a race");
        if (trackLength <= 0)
            throw new IllegalArgumentException("The track must be at least 1 meter long");

        // Init teams
        this.numberOfTeams = numberOfTeams;
        this.teams = new ArrayList<>();
        this.finalClassification = new ArrayList<>();
        for (int i = 1; i <= numberOfTeams; i++)
            this.teams.add(new Team(i));

        // Init track data
        this.trackLength = trackLength;
        this.assessmentInterval = 2.0;
    }

    /*=======================================
     * PRIVATE METHODS
     *=======================================*/

    private boolean isCompleted() {
        return this.teams.size() == 0;
    }

    /* Sort teams by arrival */
    private void sortTeams(List<Team> list) {
        Collections.sort(list, new TeamsComparator());
    }

    private ArrayList<Team> assessment() {
        // Teams who crossed the finish line at this assessment
        ArrayList<Team> finished = new ArrayList<>();

        sortTeams(this.teams);

        // Using iterator for a safe 'in place' remove
        for (Iterator<Team> iterator = this.teams.iterator(); iterator.hasNext(); /*None*/) {
            Team t = iterator.next();

            t.calculateNewPosition(this.trackLength, this.assessmentInterval);

            // If the car has crossed the finish line
            if (Math.abs(this.trackLength - t.getCurrentPosition()) <= 0.001) {
                finished.add(t);
                iterator.remove();
            }
            // Otherwise check if there is anyone else close to me, and i'm last
            else {
                t.checkPosition((ArrayList<Team>) this.teams);
            }
        }

        return finished;
    }

    /*=======================================
     * GETTERS AND SETTERS
     *=======================================*/
    public int getNumberOfTeams(){
        return this.numberOfTeams;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public double getTrackLength() {
        return trackLength;
    }

    public double getAssessmentInterval() {
        return assessmentInterval;
    }

    public List<Team> getFinalClassification() {
        return this.finalClassification;
    }

    /*=======================================
     * PUBLIC METHODS
     *=======================================*/

    public void runRace() {

        // Repeat the assessment as long as there is at least 1 car in the race
        while (!this.isCompleted()) {
            //System.out.println("------------------------------");
            List<Team> crossedFinishLine = this.assessment();
            this.sortTeams(crossedFinishLine);

            // Print those who crossed the finish line during this assessment
            for (Team t : crossedFinishLine) {
                this.finalClassification.add(t); // Only in case one want the list of the final classification
                System.out.println(t.toString());
            }
        }
    }

    /*=======================================
     * MAIN
     *=======================================*/

    public static void main(String[] args) {

        // Get args
        if(args.length != 2)
            throw new IllegalArgumentException("Usage: java Formula1 \'numberOfTeams\' \'trackLength\'");

        int numberOfTeams = 0;
        double trackLength = 0;

        try {
            numberOfTeams = Integer.parseInt(args[0]);
        }catch (NumberFormatException nfe){
            System.out.println("The first argument must be an integer.");
        }

        try {
            trackLength = Double.parseDouble(args[1]);
        }catch (NumberFormatException nfe){
            System.out.println("The second argument must be an double.");
        }

        // RUN RACE !!!
        new Race(numberOfTeams, trackLength).runRace();
    }
}
