package Logic.Utils;


public class Physics {

    /*=======================================
    * CONSTRUCTORS
    *=======================================*/
    public Physics() {

    }

    /*=======================================
     * PRIVATE METHODS
     *=======================================*/


    /*=======================================
     * GETTERS AND SETTERS
     *=======================================*/


    /*=======================================
     * PUBLIC METHODS
     *=======================================*/

    /**
     * Return how much time it will take to reach the top speed
     *
     * @param topSpeed
     * @param currentSpeed
     * @param acceleration
     * @return time is seconds
     */
    public double timeToConstantSpeed(double topSpeed, double currentSpeed, double acceleration) {
        return (topSpeed - currentSpeed) / acceleration;
    }

    /**
     * Returns how much space will be done in accelerated motion
     *
     * @param intervalLength
     * @param currentSpeed
     * @param acceleration
     * @return the space done in meters
     */
    public double spaceAccelarated(double intervalLength, double currentSpeed, double acceleration) {
        return (0.5 * acceleration * intervalLength * intervalLength) + (currentSpeed * intervalLength);
    }

    /**
     * Returns how much space will be done at constant speed
     *
     * @param intervalLength
     * @param currentSpeed
     * @return the space done in meters
     */
    public double spaceAtConstantSpeed(double intervalLength, double currentSpeed) {
        return (currentSpeed * intervalLength);
    }

    /**
     * Returns the time needed to do a certain space in accelerated motion
     *
     * @param space
     * @param currentSpeed
     * @param acceleration
     * @return the time is seconds
     * @throws IllegalArgumentException when the time is negative
     */
    public double timeToPositionInAcceleration(double space, double currentSpeed, double acceleration) throws IllegalArgumentException {
        QuadraticSolver qs = new QuadraticSolver(0.5 * acceleration, currentSpeed, -space);
        qs.solve();

        if (qs.getX1() < 0 && qs.getX2() < 0)
            throw new IllegalArgumentException("Something wrong in the input!");

        if (qs.getX1() >= 0)
            return qs.getX1();
        return qs.getX2();
    }

    /**
     * Returns the time needed to do a certain space at constant speed
     *
     * @param space
     * @param currentSpeed
     * @return the time in seconds
     */
    public double timeToPositionAtConstantSpeed(double space, double currentSpeed) {
        return space / currentSpeed;
    }

    /**
     * Returns the final speed in the space
     *
     * @param space
     * @param acceleration
     * @param initialSpeed
     * @return speed in m/s
     */
    public double speedFinalInAcceleratedMotion(double space, double acceleration, double initialSpeed) {
        double vfSquare = 2 * space * acceleration + initialSpeed * initialSpeed;
        if (vfSquare < 0)
            throw new IllegalArgumentException("Input error");
        if (Math.sqrt(vfSquare) <= initialSpeed)
            throw new IllegalArgumentException("Deceleration and constant speed not allowed");
        return Math.sqrt(vfSquare);
    }
}
