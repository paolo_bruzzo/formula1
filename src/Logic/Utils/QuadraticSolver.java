package Logic.Utils;


public class QuadraticSolver {

    /*=======================================
     * ATTRIBUTES
     *=======================================*/
    private double a;
    private double b;
    private double c;

    private double x1;
    private double x2;

    /*=======================================
     * CONSTRUCTORS
     *=======================================*/
    public QuadraticSolver(double a, double b, double c) {
        if (a == 0)
            throw new IllegalArgumentException("This is not a quadratic equation");
        this.a = a;
        this.b = b;
        this.c = c;
        this.x1 = 0.0;
        this.x2 = 0.0;
    }

    /*=======================================
     * PRIVATE METHODS
     *=======================================*/
    private boolean isPositiveDeterminant() {
        return (this.b * this.b - 4 * this.a * this.c) >= 0;
    }

    /*=======================================
     * GETTERS AND SETTERS
     *=======================================*/

    /**
     * @return root 1
     */
    public double getX1() {
        return this.x1;
    }

    /**
     * @return root 2
     */
    public double getX2() {
        return this.x2;
    }

    /*=======================================
     * PUBLIC METHODS
     *=======================================*/

    /**
     * Solve the equation
     */
    public void solve() {
        if (!isPositiveDeterminant())
            throw new IllegalArgumentException("The roots are not real");
        double rad = Math.sqrt(this.b * this.b - 4 * this.a * this.c);
        this.x1 = (-this.b + rad) / (2 * this.a);
        this.x2 = (-this.b - rad) / (2 * this.a);
    }

}
