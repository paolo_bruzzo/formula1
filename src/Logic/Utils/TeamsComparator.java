package Logic.Utils;

import Elements.Team;

import java.util.Comparator;

/* Sorting comparator */
public class TeamsComparator implements Comparator<Team> {

    @Override
    public int compare(Team t1, Team t2) {
        // Rounded
        double p1 = Math.round(t1.getCurrentPosition() * 100) / 100;
        double p2 = Math.round(t2.getCurrentPosition() * 100) / 100;
        if (p1 != p2)
            return Double.compare(p2, p1);
        return Double.compare(t1.getCurrentTime(), t2.getCurrentTime());
    }

}