package UnitTests;

import Elements.Team;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class TeamTest {

    Team t1;
    List<Team> lt;
    double trackLength1;
    double trackLength2;

    @Before
    public void setUp() throws Exception {
        t1 = new Team(1);

        lt = new ArrayList<>();
        lt.add(t1);
        lt.add(new Team(2));
        lt.add(new Team(3));

        trackLength1 = 10;
        trackLength2 = 1000;
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void calculateNewPosition1() throws Exception {

        // Accelerate without crossing finish line
        t1.calculateNewPosition(trackLength1, 2);
        assertEquals(4, t1.getCar().getCurrentSpeed(), 0);
        assertEquals(4, t1.getCurrentPosition(), 0);
        assertEquals(2, t1.getCurrentTime(), 0);

        // Cross finish line in acceleration
        t1.calculateNewPosition(trackLength1, 10);
        assertEquals(6.32455, t1.getCar().getCurrentSpeed(), 0.001);
        assertEquals(trackLength1, t1.getCurrentPosition(), 0.001);
        assertEquals(3.162277, t1.getCurrentTime(), 0.001);

    }

    @Test
    public void calculateNewPosition2() throws Exception {

        // Accelerate without crossing finish line
        t1.calculateNewPosition(trackLength2, 1);
        assertEquals(2, t1.getCar().getCurrentSpeed(), 0);
        assertEquals(1, t1.getCurrentPosition(), 0);
        assertEquals(1, t1.getCurrentTime(), 0);

        // Accelerate for a while but cross the finish line linearly
        t1.calculateNewPosition(trackLength2, 50);
        assertEquals(t1.getCar().getTopSpeed(), t1.getCar().getCurrentSpeed(), 0.001);
        assertEquals(trackLength2, t1.getCurrentPosition(), 0.001);
        assertEquals(33.61111, t1.getCurrentTime(), 0.001);

    }

    @Test
    public void checkPosition() throws Exception {

        // Move of assessment 1 second
        for(Team t : lt)
            t.calculateNewPosition(trackLength2, 1);

        // Check absolute positions
        assertEquals(1, lt.get(0).getCurrentPosition(), 0);
        assertEquals(-198, lt.get(1).getCurrentPosition(), 0);
        assertEquals(-597, lt.get(2).getCurrentPosition(), 0);

        for(Team t : lt)
            t.checkPosition((ArrayList<Team>) lt);

        // Check handling not applied, and nitro applied to last car (3)
        assertFalse(lt.get(0).getDriver().iAmLast((ArrayList<Team>) lt, lt.get(0)));
        assertEquals(2, lt.get(0).getCar().getCurrentSpeed(), 0);

        assertFalse(lt.get(1).getDriver().iAmLast((ArrayList<Team>) lt, lt.get(1)));
        assertEquals(4, lt.get(1).getCar().getCurrentSpeed(), 0);

        assertTrue(lt.get(2).getDriver().iAmLast((ArrayList<Team>) lt, lt.get(2)));
        assertEquals(12, lt.get(2).getCar().getCurrentSpeed(), 0);

    }

}