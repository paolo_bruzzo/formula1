package UnitTests;

import Elements.Car;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CarTest {

    Car c1;
    Car c2;
    Car c3;

    @Before
    public void setUp() throws Exception {
        c1 = new Car(1);
        c2 = new Car(2);
        c3 = new Car(10);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void computeSpaceRun1() throws Exception {
        double length = 0;

        // Only acceleration
        length += c1.computeSpaceRun(4);
        assertEquals(16, length, 0);
        assertEquals(8, c1.getCurrentSpeed(), 0);
        length += c1.computeSpaceRun(10);
        assertEquals(196, length, 0);
        assertEquals(28, c1.getCurrentSpeed(), 0);

        // Acceleration and constant speed
        length += c1.computeSpaceRun(11);
        assertEquals(617.28395, length, 0.0001);
        assertEquals(c1.getTopSpeed(), c1.getCurrentSpeed(), 0);

        // Constant speed
        length += c1.computeSpaceRun(2);
        assertEquals(706.17283, length, 0.0001);
        assertEquals(c1.getTopSpeed(), c1.getCurrentSpeed(), 0);
    }

    @Test
    public void computeSpaceRun2() throws Exception {
        double length = 0;

        // Only acceleration
        length += c2.computeSpaceRun(2);
        assertEquals(8, length, 0);
        assertEquals(8, c2.getCurrentSpeed(), 0);
        length += c2.computeSpaceRun(8);
        assertEquals(200, length, 0);
        assertEquals(40, c2.getCurrentSpeed(), 0);

        // Both
        length += c2.computeSpaceRun(10);
        assertEquals(665.7021, length, 0.0001);
        assertEquals(c2.getTopSpeed(), c2.getCurrentSpeed(), 0);

        //Only constant
        length += c2.computeSpaceRun(1);
        assertEquals(712.9243, length, 0.0001);
        assertEquals(c2.getTopSpeed(), c2.getCurrentSpeed(), 0);

    }

    @Test
    public void computeSpaceRun3() throws Exception {
        double length = 0;

        // Only acceleration up to top speed
        length += c3.computeSpaceRun(125.0/36);
        assertEquals(120.56327, length, 0.0001);
        assertEquals(c3.getTopSpeed(), c3.getCurrentSpeed(), 0);

        // Only constant speed
        length += c3.computeSpaceRun(3.6);
        assertEquals(370.56327, length, 0.0001);
        assertEquals(c3.getTopSpeed(), c3.getCurrentSpeed(), 0);

    }

}