package UnitTests;

import Logic.Utils.Physics;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;


import static org.junit.Assert.*;


public class PhysicsTest {

    Physics p;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        p = new Physics();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void timeToPositionInAcceleration() throws Exception {
        assertEquals(10, p.timeToPositionInAcceleration(50, 0, 1), 0);
        assertEquals(1.17017, p.timeToPositionInAcceleration(10, 7.2, 2.3), 0.0001);
        assertEquals(0, p.timeToPositionInAcceleration(0, 7.2, 2.3), 0);
    }

    @Test
    public void timeToPositionInAccelerationExc1() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("This is not a quadratic equation");
        p.timeToPositionInAcceleration(100, 10, 0);
    }

    @Test
    public void timeToPositionInAccelerationExc2() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The roots are not real");
        p.timeToPositionInAcceleration(-100, 2, 2);
    }

    @Test
    public void timeToPositionInAccelerationExc3() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Something wrong in the input!");
        p.timeToPositionInAcceleration(-4, 12, 18);
    }

    @Test
    public void speedFinalInAcceleratedMotion() throws Exception {
        assertEquals(9.891410, p.speedFinalInAcceleratedMotion(10, 2.3, 7.2), 0.0001);
    }

    @Test
    public void speedFinalInAcceleratedMotionExc1() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Deceleration and constant speed not allowed");
        p.speedFinalInAcceleratedMotion(10, -1, 7.2);
    }

}