package UnitTests;

import Elements.Team;
import Logic.Race;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;

public class RaceTest {

    List<Race> lr;
    int randomTestCases;

    @Before
    public void setUp() throws Exception {

        randomTestCases = 100;
        lr = new ArrayList<>();

        /* Creates 100 random races to check the integrity constraints.*/
        for (int i = 0; i < randomTestCases; i++) {
            int numberOfTeams = new Random().nextInt(1000) + 2;
            double trackLenght = 1 + 100000 * new Random().nextDouble();
            lr.add(new Race(numberOfTeams, trackLenght));
        }
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void runRace() throws Exception {
        for (Race r : lr) {
            r.runRace();

            // Check that each assessment, actually register (in order) all the cars that crossed the line
            // (a car that crossed the line in assessment X, must have arrived before a car that crossed in assessment X+1)
            for (int i = 0; i < (r.getFinalClassification().size() - 1); i++) {
                assertTrue(r.getFinalClassification().get(i).getCurrentTime() <= r.getFinalClassification().get(i + 1).getCurrentTime());
            }

            // Check that all the cars are on the finish line at the end of the race
            // and the speed is not grater than the top speed
            for (Team t : r.getFinalClassification()) {
                assertEquals(r.getTrackLength(), t.getCurrentPosition(), 0.0001);
                assertTrue(t.getCar().getCurrentSpeed() <= t.getCar().getTopSpeed());
            }

            // Assert that all the cars actually crossed the finish line
            assertTrue(r.getFinalClassification().size() == r.getNumberOfTeams());

            System.out.println("Test " + (lr.indexOf(r) + 1) + " PASSED " +
                    "( Number Of Teams = " + r.getNumberOfTeams() + ", " +
                    "Track Length = " + r.getTrackLength()+" )");
        }
    }

}